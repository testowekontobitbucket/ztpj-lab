package pl.edu.zut.ztpjlab2.Entities;

import java.math.BigDecimal;

public class Salesman extends Employee {
    private BigDecimal commissionRate;
    private BigDecimal commissionLimitPerMonth;

    public BigDecimal getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(BigDecimal commissionRate) {
        this.commissionRate = commissionRate;
    }

    public BigDecimal getCommissionLimitPerMonth() {
        return commissionLimitPerMonth;
    }

    public void setCommissionLimitPerMonth(BigDecimal commissionLimitPerMonth) {
        this.commissionLimitPerMonth = commissionLimitPerMonth;
    }
}
