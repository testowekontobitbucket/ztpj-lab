package pl.edu.zut.ztpjlab2.Entities;

import java.math.BigDecimal;

public class Officer extends Employee {
    private BigDecimal bonus;
    private String businessCard;
    private BigDecimal costsLimit;

    public BigDecimal getBonus() {
        return bonus;
    }

    public void setBonus(BigDecimal bonus) {
        this.bonus = bonus;
    }

    public String getBusinessCard() {
        return businessCard;
    }

    public void setBusinessCard(String businessCard) {
        this.businessCard = businessCard;
    }

    public BigDecimal getCostsLimit() {
        return costsLimit;
    }

    public void setCostsLimit(BigDecimal costsLimit) {
        this.costsLimit = costsLimit;
    }
}
