package pl.edu.zut.ztpjlab2.Entities;

import pl.edu.zut.ztpjlab2.Enum.PositionEnum;

import java.io.Serializable;
import java.math.BigDecimal;

public class Employee implements Serializable {
    private String pesel;
    private String name;
    private String surname;
    private PositionEnum position;
    private BigDecimal sallary;
    private Integer businessPhone;

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public PositionEnum getPosition() {
        return position;
    }

    public void setPosition(PositionEnum position) {
        this.position = position;
    }

    public BigDecimal getSallary() {
        return sallary;
    }

    public void setSallary(BigDecimal sallary) {
        this.sallary = sallary;
    }

    public Integer getBusinessPhone() {
        return businessPhone;
    }

    public void setBusinessPhone(Integer businessPhone) {
        this.businessPhone = businessPhone;
    }
}
