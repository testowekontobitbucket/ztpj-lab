package pl.edu.zut.ztpjlab2.View;

import pl.edu.zut.ztpjlab2.DAOs.OfficerDAO;
import pl.edu.zut.ztpjlab2.DAOs.SalesmanDAO;
import pl.edu.zut.ztpjlab2.Entities.Employee;
import pl.edu.zut.ztpjlab2.Entities.Officer;
import pl.edu.zut.ztpjlab2.Entities.Salesman;

import java.util.LinkedList;
import java.util.Scanner;

public class EmployeeList extends View implements ViewInterface {
    public void render() {
        OfficerDAO officerDAO = new OfficerDAO();
        LinkedList<Employee> officers = officerDAO.readAll();

        SalesmanDAO salesmanDAO = new SalesmanDAO();
        LinkedList<Employee> salesmans = salesmanDAO.readAll();

        LinkedList<Employee> employees = new LinkedList<>();
        employees.addAll(officers);
        employees.addAll(salesmans);

        int iteration = 1;

        for (Employee employee : employees) {
            printEmployee(employee);

            switch (employee.getPosition()) {
                case OFFICER:
                    printOfficer(employee);
                    break;
                case SALESMAN:
                    printSalesman(employee);
                    break;
            }

            System.out.println("[Pozycja: " + iteration + "/" + employees.size() + "]");
            System.out.println("[Enter] - następny");
            System.out.println("[Q] - powrót");
            iteration++;

            Scanner scanner = new Scanner(System.in);
            String input = scanner.nextLine();
            if(input.equals("Q")) break;
        }
    }

    private void printEmployee(Employee employee) {
        System.out.println("1. Lista pracowników");
        System.out.println("Identyfikator PESEL: " + employee.getPesel());
        System.out.println("Imię: " + employee.getName());
        System.out.println("Nazwisko: " + employee.getSurname());
        System.out.println("Stanowisko: " + employee.getPosition());
        System.out.println("Wynagrodzenie (zł): " + employee.getSallary());
        System.out.println("Telefon służbowy numer: " +  employee.getBusinessPhone());
    }

    private void printOfficer(Employee employee) {
        System.out.println("Dodatek służbowy (zł): " + ((Officer)employee).getBonus());
        System.out.println("Karta służbowa numer: " + ((Officer)employee).getBusinessCard());
        System.out.println("Limit kosztów/miesiąc (zł): " + ((Officer)employee).getCostsLimit());
    }

    private void printSalesman(Employee employee) {
        System.out.println("Prowizja (%): " + ((Salesman)employee).getCommissionRate());
        System.out.println("Limit prowizji/miesiąc (zł): " + ((Salesman)employee).getCommissionLimitPerMonth());
    }
}
