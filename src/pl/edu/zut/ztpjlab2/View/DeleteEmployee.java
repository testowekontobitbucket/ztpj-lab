package pl.edu.zut.ztpjlab2.View;

import pl.edu.zut.ztpjlab2.DAOs.OfficerDAO;
import pl.edu.zut.ztpjlab2.DAOs.SalesmanDAO;
import pl.edu.zut.ztpjlab2.Entities.Employee;
import pl.edu.zut.ztpjlab2.Entities.Officer;
import pl.edu.zut.ztpjlab2.Entities.Salesman;
import pl.edu.zut.ztpjlab2.Service.EmployeeService;

public class DeleteEmployee extends View implements ViewInterface {
    public void render() {
        System.out.println("3. Usuń pracownika.");
        System.out.println("Podaj identyfikator PESEL");

        String pesel = this.readInput();
        Employee employee = EmployeeService.findByPesel(pesel);
        if (employee == null) {
            System.out.println("Nie znaleziono pracownika o numerze PESEL " + pesel);
            return;
        }

        this.printEmployee(employee);

        switch (employee.getPosition()) {
            case OFFICER:
                printOfficer(employee);
                break;
            case SALESMAN:
                printSalesman(employee);
                break;
        }

        System.out.println("[Enter] - potwierdź");
        System.out.println("[Q] - porzuć");
        String input = readInput();

        if(input.equals("Q")) return;

        OfficerDAO officerDAO = new OfficerDAO();
        Boolean officerDeleted = officerDAO.delete(employee);

        SalesmanDAO salesmanDAO = new SalesmanDAO();
        Boolean salesmanDeleted = salesmanDAO.delete(employee);

        if(officerDeleted || salesmanDeleted) {
            System.out.println("Usunięto pracownika o nuemrze PESEL " + pesel);
        }
        else {
            System.out.println("Nie usunieto pracownika o numerze PESEL " + pesel);
        }
    }

    private void printEmployee(Employee employee) {
        System.out.println("1. Lista pracowników");
        System.out.println("Identyfikator PESEL: " + employee.getPesel());
        System.out.println("Imię: " + employee.getName());
        System.out.println("Nazwisko: " + employee.getSurname());
        System.out.println("Stanowisko: " + employee.getPosition());
        System.out.println("Wynagrodzenie (zł): " + employee.getSallary());
        System.out.println("Telefon służbowy numer: " +  employee.getBusinessPhone());
    }

    private void printOfficer(Employee employee) {
        System.out.println("Dodatek służbowy (zł): " + ((Officer)employee).getBonus());
        System.out.println("Karta służbowa numer: " + ((Officer)employee).getBusinessCard());
        System.out.println("Limit kosztów/miesiąc (zł): " + ((Officer)employee).getCostsLimit());
    }

    private void printSalesman(Employee employee) {
        System.out.println("Prowizja (%): " + ((Salesman)employee).getCommissionRate());
        System.out.println("Limit prowizji/miesiąc (zł): " + ((Salesman)employee).getCommissionLimitPerMonth());
    }
}