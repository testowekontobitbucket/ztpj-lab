package pl.edu.zut.ztpjlab2.View;

public interface ViewInterface {
    void render();
}
