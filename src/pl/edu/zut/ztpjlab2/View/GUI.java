package pl.edu.zut.ztpjlab2.View;

import pl.edu.zut.ztpjlab2.Entities.Employee;

import java.util.Scanner;

public class GUI {
    public void showMenu() {
        this.printMenu();
        this.readMenuChoice();
    }

    private void printMenu() {
        System.out.println("Menu");
        System.out.println("1. Lista pracowników");
        System.out.println("2. Dodaj pracownika");
        System.out.println("3. Usuń pracownika");
        System.out.println("4. Kopia zapasowa");
        System.out.println("5. Pobierz dane z sieci");

        System.out.print("Wybór: ");
    }

    private void readMenuChoice() {
        switch (readInput()) {
            case 1:
                EmployeeList employeeList = new EmployeeList();
                employeeList.render();
                break;
            case 2:
                AddEmployee addEmployee = new AddEmployee();
                addEmployee.render();
                break;
            case 3:
                DeleteEmployee deleteEmployee = new DeleteEmployee();
                deleteEmployee.render();
                break;
            case 4:
                EmployeesDataBackup employeesDataBackup = new EmployeesDataBackup();
                employeesDataBackup.render();
                break;
            case 5:
                FetchDataFromWeb fetchDataFromWeb = new FetchDataFromWeb();
                fetchDataFromWeb.render();
                break;
        }
    }

    private Integer readInput() {
        Scanner scanner = new Scanner(System.in);
        return Integer.parseInt(scanner.nextLine());
    }
}
