package pl.edu.zut.ztpjlab2.View;

import pl.edu.zut.ztpjlab2.Service.EmployeeBackupService;

public class EmployeesDataBackup extends View implements ViewInterface {
    private String compressionType;
    private String fileName;

    public void render() {
        System.out.println("4. Kopia zapasowa");
        System.out.println("[Z]achowaj / [O]dtwórz");

        String mode = this.readInput();

        switch (mode) {
            case "Z":
                renderExport();
                break;
            case "O":
                renderImport();
                break;
        }

        System.out.println("[Enter] - potwierdź");
        System.out.println("[Q] - porzuć");
        input = readInput();

        if(input.equals("Q")) return;

        switch (mode) {
            case "Z":
                EmployeeBackupService.exportData(fileName, compressionType);
                //TODO: dodaj zip i gzip
                break;
            case "O":
                EmployeeBackupService.importData(fileName);
                break;
        }
    }

    private void renderExport() {
        System.out.println("Kompresja [G]zip/[Z]ip");
        compressionType = this.readInput();
        System.out.println("Nazwa pliku");
        fileName = this.readInput();
    }

    private void renderImport() {
        System.out.println("Nazwa pliku");
        fileName = this.readInput();
    }
}
