package pl.edu.zut.ztpjlab2.View;

import pl.edu.zut.ztpjlab2.DAOs.OfficerDAO;
import pl.edu.zut.ztpjlab2.DAOs.SalesmanDAO;
import pl.edu.zut.ztpjlab2.Entities.Employee;
import pl.edu.zut.ztpjlab2.Entities.Officer;
import pl.edu.zut.ztpjlab2.Entities.Salesman;
import pl.edu.zut.ztpjlab2.Enum.PositionEnum;
import pl.edu.zut.ztpjlab2.Service.EmployeeService;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.Scanner;

public class AddEmployee extends View implements ViewInterface {

    private Employee employee;

    public void render() {

        System.out.println("2. Dodaj pracownika");

        System.out.println("[D]yrektor/[H]handlowiec: ");
        input = this.readInput();
        if(input.equals("D")) {
            employee = new Officer();
            employee.setPosition(PositionEnum.OFFICER);
        }
        else if (input.equals("H")) {
            employee = new Salesman();
            employee.setPosition(PositionEnum.SALESMAN);
        }

        this.fetchGeneralData();

        switch (input) {
            case "D":
                this.fetchOfficerData();
                OfficerDAO officerDAO = new OfficerDAO();
                officerDAO.create(employee);
                break;
            case "H":
                this.fetchSalesmanData();
                SalesmanDAO salesmanDAO = new SalesmanDAO();
                salesmanDAO.create(employee);
                break;
        }
    }

    private void fetchOfficerData() {
        System.out.println("Dodatek służbowy (zł): ");
        ((Officer)employee).setBonus(new BigDecimal(this.readInput()));

        System.out.println("Karta służbowa numer: ");
        ((Officer)employee).setBusinessCard(this.readInput());

        System.out.println("Limit kosztów/miesiąc (zł): ");
        ((Officer)employee).setCostsLimit(new BigDecimal(this.readInput()));
    }

    private void fetchSalesmanData() {
        System.out.println("Prowizja (%): ");
        ((Salesman)employee).setCommissionRate(new BigDecimal(this.readInput()));

        System.out.println("Limit prowizji/miesiąc (zł): ");
        ((Salesman)employee).setCommissionLimitPerMonth(new BigDecimal(this.readInput()));
    }

    private void fetchGeneralData() {
        System.out.println("Identyfikator PESEL: ");
        employee.setPesel(this.readInput());
        try {
            this.validatePesel(employee.getPesel());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            this.render();
        }

        System.out.println("Imię: ");
        employee.setName(this.readInput());

        System.out.println("Nazwisko: ");
        employee.setSurname(this.readInput());

        System.out.println("Wynagrodzenie: ");
        employee.setSallary(new BigDecimal(this.readInput()));

        System.out.println("Telefon służbowy numer: ");
        employee.setBusinessPhone(Integer.parseInt(this.readInput()));
    }

    private void validatePesel(String pesel) throws Exception {
        if(EmployeeService.findByPesel(pesel) != null) throw new Exception("PESEL should be unique.");
    }
}
