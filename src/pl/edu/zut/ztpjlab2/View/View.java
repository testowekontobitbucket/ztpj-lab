package pl.edu.zut.ztpjlab2.View;

import java.util.Scanner;

public class View {
    protected String input;

    public String readInput() {
        Scanner scanner = new Scanner(System.in);
        return  scanner.nextLine();
    }
}
