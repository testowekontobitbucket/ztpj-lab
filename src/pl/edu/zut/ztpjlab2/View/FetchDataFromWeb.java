package pl.edu.zut.ztpjlab2.View;

import pl.edu.zut.ztpjlab2.Client.SoapClientDataPersister;
import pl.edu.zut.ztpjlab2.DAOs.OfficerDAO;
import pl.edu.zut.ztpjlab2.DAOs.SalesmanDAO;
import pl.edu.zut.ztpjlab2.Entities.Employee;

import java.io.*;
import java.net.Socket;
import java.util.LinkedList;

public class FetchDataFromWeb extends View implements ViewInterface {
    private String ip;
    private String port;

    private Socket clientSocket;
    private LinkedList<Employee> employees;
    private String username;
    private String password;

    @Override
    public void render() {
        System.out.println("5. Pobierz dane z sieci");
        System.out.println("Podaj użytkownika");
        this.username = this.readInput();
        System.out.println("Poda hasło");
        if(System.console() != null)
        this.password = String.valueOf(System.console().readPassword());
        else this.password = this.readInput();
        System.out.println("Adres");
        this.ip = this.readInput();
        System.out.println("Port");
        this.port = this.readInput();

        try {
            clientSocket = new Socket(this.ip, Integer.parseInt(this.port));
            System.out.println("Ustanawianie połączenia... Sukces");
            System.out.print("Pobieranie... ");

            DataOutputStream dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());
            authenticate(dataOutputStream);

            System.out.println("Protokół [T]CP/IP czy [S]OAP?");
            switch (this.readInput()) {
                case "T":
                    tcpIpProtocol(dataOutputStream);
                    break;
                case "S":
                    SoapClientDataPersister soapClientDataPersister = new SoapClientDataPersister();
                    this.employees = soapClientDataPersister.persist();
                    break;
            }


            System.out.println("Sukces");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println("Czy zapisać pobrane dane? [T]/[N]");
        String choice = this.readInput();

        if(choice.equals("T")) {
            for (Employee employee : employees) {
                switch (employee.getPosition()) {
                    case OFFICER:
                        OfficerDAO officerDAO = new OfficerDAO();
                        officerDAO.create(employee);
                        break;
                    case SALESMAN:
                        SalesmanDAO salesmanDAO = new SalesmanDAO();
                        salesmanDAO.create(employee);
                        break;
                }
            }
        }

        System.out.println("Zapisywanie... Sukces");
        System.out.println("[ENTER] - powrót do ekranu głównego");
        this.readInput();
    }

    private void tcpIpProtocol(DataOutputStream dataOutputStream) throws IOException, ClassNotFoundException {
        dataOutputStream.writeUTF("FETCH");
        dataOutputStream.flush();

        ObjectInputStream objectInputStream = new ObjectInputStream(clientSocket.getInputStream());

        employees = new LinkedList<>();

        Employee employee;
        try {
            while((employee = (Employee) objectInputStream.readObject()) != null) {
                employees.add(employee);
                System.out.println(employee.getName());
            }
        }
        catch (EOFException ignored) {}
    }

    private void authenticate(DataOutputStream dataOutputStream) throws IOException {
        String rmiUri = "rmi://" + this.ip  + ":1099/validator";
        dataOutputStream.writeUTF(rmiUri);
        dataOutputStream.writeUTF(username);
        dataOutputStream.writeUTF(password);
        dataOutputStream.flush();
    }
}
