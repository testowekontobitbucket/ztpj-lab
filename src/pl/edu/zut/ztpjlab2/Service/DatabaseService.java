package pl.edu.zut.ztpjlab2.Service;

import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;
import org.apache.commons.dbcp2.*;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseService {
    private static DatabaseService ourInstance = new DatabaseService();

    public static DatabaseService getInstance() {
        return ourInstance;
    }

    private static Connection connection;

    private static GenericObjectPool<PoolableConnection> connectionPool;

    private DatabaseService() {
        try {
            Class.forName("com.mysql.jdbc.Driver");

            ConnectionFactory connectionFactory = new DriverManagerConnectionFactory("jdbc:mysql://localhost:3306/ztpj","ztpj", "ztpj");
            PoolableConnectionFactory poolableConnectionFactory = new PoolableConnectionFactory(connectionFactory, null);
            poolableConnectionFactory.setValidationQuery("SELECT 1");

            connectionPool = new GenericObjectPool<>(poolableConnectionFactory);
            poolableConnectionFactory.setPool(connectionPool);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
    public static Connection getConnection() {
        DataSource dataSource = new PoolingDataSource<>(connectionPool);
        try {
            connection = dataSource.getConnection();
        } catch (CommunicationsException e) {
            System.out.println(e.getMessage());
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return connection;
    }
}
