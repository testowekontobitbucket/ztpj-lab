package pl.edu.zut.ztpjlab2.Service;

import pl.edu.zut.ztpjlab2.DAOs.OfficerDAO;
import pl.edu.zut.ztpjlab2.DAOs.SalesmanDAO;
import pl.edu.zut.ztpjlab2.Entities.Employee;

import java.util.LinkedList;

public class EmployeeService {
    public static Employee findByPesel(String pesel) {
        LinkedList<Employee> employees = EmployeeService.findAll();

        for (Employee employee : employees) {
            if(employee.getPesel().equals(pesel)) {
                return employee;
            }
        }

        return null;
    }

    public static LinkedList<Employee> findAll() {
        OfficerDAO officerDAO = new OfficerDAO();
        LinkedList<Employee> employees = officerDAO.readAll();
        SalesmanDAO salesmanDAO = new SalesmanDAO();
        employees.addAll(salesmanDAO.readAll());
        return employees;
    }
}
