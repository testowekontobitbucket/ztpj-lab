package pl.edu.zut.ztpjlab2.Service;

import pl.edu.zut.ztpjlab2.DAOs.OfficerDAO;
import pl.edu.zut.ztpjlab2.DAOs.SalesmanDAO;
import pl.edu.zut.ztpjlab2.Entities.Employee;

import java.io.*;
import java.util.LinkedList;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class EmployeeBackupService {
    public static void exportData(String filename, String compressionType) {
        LinkedList<Employee> employees = EmployeeService.findAll();

        String extension = "zip";
        if(compressionType.equals("G")) extension = "gzip";

        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                    compressionType.equals("G")
                            ? new GZIPOutputStream(new FileOutputStream(filename + "." + extension))
                            : new ZipOutputStream(new FileOutputStream(filename + "." + extension))
            );
            for (Employee employee : employees) {
                objectOutputStream.writeObject(employee);
            }
            objectOutputStream.flush();
            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void importData(String filename) {
        try {
            String extension = filename.substring(filename.indexOf(".") + 1); //get file extension
            String compressionType = "Z";
            if(extension.equals("gzip")) compressionType = "G";

            ObjectInputStream objectInputStream = null;
            try {
                objectInputStream = new ObjectInputStream(
                        compressionType.equals("G")
                                ? new GZIPInputStream(new FileInputStream(filename))
                                    : new ZipInputStream(new FileInputStream(filename))
                );
            } catch (IOException e) {
                e.printStackTrace();
            }
            Employee employee;

            while((employee = (Employee) objectInputStream.readObject()) != null) {
                switch (employee.getPosition()) {
                    case OFFICER:
                        OfficerDAO officerDAO = new OfficerDAO();
                        officerDAO.create(employee);
                        break;
                    case SALESMAN:
                        SalesmanDAO salesmanDAO = new SalesmanDAO();
                        salesmanDAO.create(employee);
                        break;
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
