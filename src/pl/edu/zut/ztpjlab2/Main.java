package pl.edu.zut.ztpjlab2;

import pl.edu.zut.ztpjlab2.Server.ServerDaemonService;
import pl.edu.zut.ztpjlab2.Server.SoapDaemonService;
import pl.edu.zut.ztpjlab2.View.GUI;

public class Main {

    public static void main(String[] args) {
        startSoapServer();
        startServer();

        GUI gui = new GUI();
        gui.showMenu();
    }

    private static void startServer() {
        Runnable runnable = () -> {
            ServerDaemonService serverDaemonService = new ServerDaemonService();
            serverDaemonService.start(1234);
        };
        Thread serverThread = new Thread(runnable);
        serverThread.start();
    }

    private static void startSoapServer() {
        Runnable runnable = () -> {
            SoapDaemonService soapDaemonService = new SoapDaemonService();
            soapDaemonService.start();
        };
        Thread serverThread = new Thread(runnable);
        serverThread.start();
    }
}