package pl.edu.zut.ztpjlab2.RMI;

import pl.edu.zut.ztpjlab2.Server.ServerDaemonService;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.ldap.InitialLdapContext;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;

public class Validator extends UnicastRemoteObject implements ValidatorInterface {
    private HashMap<String, String> members;

    public Validator() throws RemoteException {
        members = new HashMap<>();
        members.put("admin","123");
    }

    @Override
    public String validate(String username, String password) throws RemoteException {
        if(username.isEmpty() || password.isEmpty()) try {
            throw new Exception("Empty login data.");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        if(existsInInternalDB(username, password) || existsInLDAP(username, password)) {
            String token = Base64.getEncoder().encode(UUID.randomUUID().toString().getBytes()).toString();
            ServerDaemonService.tokens.put(token, System.currentTimeMillis());
            return token;
        }

        return null;
    }

    private boolean existsInLDAP(String username, String password) {

        Hashtable<String, String> env = new Hashtable<>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, "ldap://82.145.72.13:389");
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, "wipsad\\" + username);
        env.put(Context.SECURITY_CREDENTIALS, password);

        try
        {
            new InitialLdapContext(env, null);
            return true;
        }
        catch (NamingException e)
        {
            System.out.println(e.getMessage());
        }

        return false;
    }

    private boolean existsInInternalDB(String username, String password) {
        return members.containsKey(username) && members.get(username).equals(password);
    }
}
