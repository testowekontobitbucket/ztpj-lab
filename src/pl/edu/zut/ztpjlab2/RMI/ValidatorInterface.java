package pl.edu.zut.ztpjlab2.RMI;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ValidatorInterface extends Remote {
    String validate(String username, String password) throws RemoteException;
}
