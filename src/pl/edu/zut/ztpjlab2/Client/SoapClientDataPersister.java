package pl.edu.zut.ztpjlab2.Client;

import pl.edu.zut.ztpjlab2.DAOs.OfficerDAO;
import pl.edu.zut.ztpjlab2.DAOs.SalesmanDAO;
import pl.edu.zut.ztpjlab2.Entities.Employee;
import pl.edu.zut.ztpjlab2.Entities.Officer;
import pl.edu.zut.ztpjlab2.Entities.Salesman;
import pl.edu.zut.ztpjlab2.Enum.PositionEnum;
import pl.edu.zut.ztpjlab2.JAXB.Employees;
import pl.edu.zut.ztpjlab2.Server.SoapDaemonService;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;

import static pl.edu.zut.ztpjlab2.Enum.PositionEnum.OFFICER;

public class SoapClientDataPersister {
    private LinkedList<Employee> employees;

    public LinkedList<Employee> persist() {
        employees = new LinkedList<>();
        ShareEmployeesWebServiceClient shareEmployeesWebServiceClient = new ShareEmployeesWebServiceClient();
        String xml = shareEmployeesWebServiceClient.fetchXml();

        JAXBContext jContext = null;
        try {
            jContext = JAXBContext.newInstance(Employees.class);
            Unmarshaller unmarshallerObj = jContext.createUnmarshaller();
            StringReader stringReader = new StringReader(xml);
            Employees jaxbEmployees=(Employees)unmarshallerObj.unmarshal(stringReader);
            mapJaxbEmployeeToEntity(jaxbEmployees);
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        return employees;
    }

    private void mapJaxbEmployeeToEntity(Employees jaxbEmployees) {
        List<pl.edu.zut.ztpjlab2.JAXB.Employee> jaxbEmployeesList = jaxbEmployees.getEmployee();
        for (pl.edu.zut.ztpjlab2.JAXB.Employee jaxbEmployee : jaxbEmployeesList) {
            Employee employee = null;
            switch (jaxbEmployee.getPosition()) {
                case "OFFICER":
                    employee = new Officer();
                    ((Officer)employee).setBonus(jaxbEmployee.getBonus());
                    ((Officer)employee).setBusinessCard(jaxbEmployee.getBusinessCard());
                    ((Officer)employee).setCostsLimit(jaxbEmployee.getCostsLimit());
                    break;
                case "SALESMAN":
                    employee = new Salesman();
                    ((Salesman)employee).setCommissionLimitPerMonth(jaxbEmployee.getCommissionLimitPerMonth());
                    ((Salesman)employee).setCommissionRate(jaxbEmployee.getCommissionRate());
                    break;
            }

            employee.setBusinessPhone(jaxbEmployee.getBusinessPhone().intValue());
            employee.setName(jaxbEmployee.getName());
            employee.setPesel(jaxbEmployee.getPesel());
            employee.setPosition(PositionEnum.valueOf(jaxbEmployee.getPosition()));
            employee.setSallary(jaxbEmployee.getSallary());
            employee.setSurname(jaxbEmployee.getSurname());

            employees.add(employee);
        }
    }

}
