package pl.edu.zut.ztpjlab2.Client;

import javax.xml.ws.WebServiceRef;

public class ShareEmployeesWebServiceClient {
    public String fetchXml() {
        ShareEmployeesWebServiceService shareEmployeesWebServiceService = new ShareEmployeesWebServiceService();
        ShareEmployeesWebService shareEmployeesWebService = shareEmployeesWebServiceService.getShareEmployeesWebServicePort();
        return shareEmployeesWebService.getEmployees();
    }
}
