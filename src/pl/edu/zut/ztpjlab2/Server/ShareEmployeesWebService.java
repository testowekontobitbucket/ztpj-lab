package pl.edu.zut.ztpjlab2.Server;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public class ShareEmployeesWebService {
    @WebMethod
    public String getEmployees() {
        return SoapDaemonService.xml;
    }
}
