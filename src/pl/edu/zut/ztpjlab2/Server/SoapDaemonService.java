package pl.edu.zut.ztpjlab2.Server;

import pl.edu.zut.ztpjlab2.Entities.Employee;
import pl.edu.zut.ztpjlab2.Entities.Officer;
import pl.edu.zut.ztpjlab2.Entities.Salesman;
import pl.edu.zut.ztpjlab2.JAXB.Employees;
import pl.edu.zut.ztpjlab2.Service.EmployeeService;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.ws.Endpoint;
import java.io.StringWriter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class SoapDaemonService {
    public static String xml;

    public void start() {

        LinkedList<pl.edu.zut.ztpjlab2.Entities.Employee> employeesList = EmployeeService.findAll();
        List<pl.edu.zut.ztpjlab2.JAXB.Employee> jaxbEmployeeList = new ArrayList<>();
        for (Employee employeeElement : employeesList) {
            pl.edu.zut.ztpjlab2.JAXB.Employee employee = new pl.edu.zut.ztpjlab2.JAXB.Employee();

            mapEmployeeClasses(employeeElement, employee);

            jaxbEmployeeList.add(employee);
        }

        Employees employees = new Employees();
        employees.setEmployee(jaxbEmployeeList);

        try{
            JAXBContext jContext = JAXBContext.newInstance(Employees.class);
            Marshaller marshallObj = jContext.createMarshaller();
            marshallObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            StringWriter stringWriter = new StringWriter();
            marshallObj.marshal(employees, stringWriter);
            xml = stringWriter.toString();
        } catch(Exception e) {
            e.printStackTrace();
        }

        String bindingUrl = "http://localhost:1253/getEmployees";
        ShareEmployeesWebService webService = new ShareEmployeesWebService();
        Endpoint.publish(bindingUrl, webService);
    }

    private void mapEmployeeClasses(Employee employeeElement, pl.edu.zut.ztpjlab2.JAXB.Employee employee) {
        employee.setBusinessPhone(BigInteger.valueOf(employeeElement.getBusinessPhone()));
        employee.setName(employeeElement.getName());
        employee.setPesel(employeeElement.getPesel());
        employee.setPosition(String.valueOf(employeeElement.getPosition()));
        employee.setSallary(employeeElement.getSallary());
        employee.setSurname(employeeElement.getSurname());

        switch (employeeElement.getPosition()) {
            case OFFICER:
                employee.setBonus(((Officer)employeeElement).getBonus());
                employee.setBusinessCard(((Officer)employeeElement).getBusinessCard());
                employee.setCostsLimit(((Officer)employeeElement).getCostsLimit());
                break;
            case SALESMAN:
                employee.setCommissionLimitPerMonth(((Salesman)employeeElement).getCommissionLimitPerMonth());
                employee.setCommissionRate(((Salesman)employeeElement).getCommissionRate());
                break;
        }
    }
}
