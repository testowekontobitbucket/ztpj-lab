package pl.edu.zut.ztpjlab2.Server;

import pl.edu.zut.ztpjlab2.Entities.Employee;
import pl.edu.zut.ztpjlab2.RMI.Validator;
import pl.edu.zut.ztpjlab2.RMI.ValidatorInterface;
import pl.edu.zut.ztpjlab2.Service.EmployeeService;

import java.io.*;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.AccessDeniedException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.HashMap;
import java.util.LinkedList;

public class ServerDaemonService {

    private ServerSocket serverSocket;
    public static HashMap<String, Long> tokens;

    public void start(int port) {
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }


        while (true) {
            try {
                Socket socket = serverSocket.accept();
                DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());

                tokens = new HashMap<>();
                Validator validator = null;
                try {
                    validator = new Validator();
                    LocateRegistry.createRegistry(1099); //run registry in the same vm
                    Naming.rebind("validator", validator);
                } catch (RemoteException | MalformedURLException e) {
                    e.printStackTrace();
                }

                String rmiUri = dataInputStream.readUTF();
                ValidatorInterface remote = (ValidatorInterface) Naming.lookup(rmiUri);
                String username = dataInputStream.readUTF();
                String password = dataInputStream.readUTF();
                String token = remote.validate(username, password);

                if (validateToken(token)) throw new AccessDeniedException("Not authorized.");

                ServerDaemonService.tokens.remove(token);
                System.out.println("Logowanie pomyślne");

                String message = dataInputStream.readUTF();

                if (message.equals("FETCH")) {
                    ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());

                    LinkedList<Employee> employees = EmployeeService.findAll();

                    for (Employee employee : employees) {
                        objectOutputStream.writeObject(employee);
                    }
                    objectOutputStream.flush();
                    objectOutputStream.close();
                }
            } catch (IOException | NotBoundException e) {
                e.printStackTrace();
            }

        }
    }

    private boolean validateToken(String token) {
        if (token == null) {
            System.out.println("Błędne dane logowania");
            return true;
        }
        if(tokenExpired(token)) {
            System.out.println("Token wygasł.");
            return true;
        }
        return false;
    }

    private boolean tokenExpired(String token) {
        long expiryTime = ServerDaemonService.tokens.get(token);
        long difference = System.currentTimeMillis() - expiryTime;
        return difference > 120000;
    }
}