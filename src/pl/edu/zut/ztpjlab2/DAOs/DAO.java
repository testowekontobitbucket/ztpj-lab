package pl.edu.zut.ztpjlab2.DAOs;

import java.util.LinkedList;
import java.util.Optional;

public interface DAO<T> {

    void create(T t);

    Optional<T> read(long id);

    void update(T t, String[] params);

    Boolean delete(T t);

    LinkedList<T> readAll();
}
