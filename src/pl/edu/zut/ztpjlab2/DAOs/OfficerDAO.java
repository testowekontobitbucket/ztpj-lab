package pl.edu.zut.ztpjlab2.DAOs;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import pl.edu.zut.ztpjlab2.Entities.Employee;
import pl.edu.zut.ztpjlab2.Entities.Officer;
import pl.edu.zut.ztpjlab2.Enum.PositionEnum;
import pl.edu.zut.ztpjlab2.Service.DatabaseService;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.Optional;

public class OfficerDAO implements DAO {
    private final String DB_NAME = "officer";

    @Override
    public void create(Object o) {
        Officer officer = (Officer) o;

        PreparedStatement statement;
        try {
            statement = DatabaseService.getConnection().prepareStatement(
                    "INSERT INTO " + DB_NAME +
                            "(`pesel`, `name`, `surname`, `position`, `sallary`, `business_phone`, `bonus`, `business_card`, `costs_limit`) " +
                            " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"
            );
            statement.setString(1, officer.getPesel());
            statement.setString(2, officer.getName());
            statement.setString(3, officer.getSurname());
            statement.setString(4, officer.getPosition().name());
            statement.setBigDecimal(5, officer.getSallary());
            statement.setInt(6, officer.getBusinessPhone());
            statement.setBigDecimal(7, officer.getBonus());
            statement.setString(8, officer.getBusinessCard());
            statement.setBigDecimal(9, officer.getCostsLimit());

            statement.executeUpdate();

        }
        catch (MySQLIntegrityConstraintViolationException e) {
            System.out.println(e.getMessage());
        }
        catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public Optional read(long id) {
        //not required
        return Optional.empty();
    }

    @Override
    public void update(Object o, String[] params) {
        //not required
    }

    @Override
    public Boolean delete(Object o) {
        PreparedStatement statement;

        try {
            statement = DatabaseService.getConnection().prepareStatement(
                    "DELETE FROM " + DB_NAME + " WHERE pesel = ?"
            );
            statement.setString(1, ((Employee)o).getPesel());

            int result = statement.executeUpdate();
            return result == 1 ? Boolean.TRUE : Boolean.FALSE;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Boolean.FALSE;
    }

    @Override
    public LinkedList readAll() {
        PreparedStatement statement;
        ResultSet resultSet;
        LinkedList<Officer> oficerList = new LinkedList<>();


        try {
            statement = DatabaseService.getConnection().prepareStatement(
                    "SELECT * FROM " + DB_NAME
            );
            resultSet = statement.executeQuery();

            while(resultSet.next()) {
                Officer officer = new Officer();
                officer.setBonus(resultSet.getBigDecimal("bonus"));
                officer.setBusinessCard(resultSet.getString("business_card"));
                officer.setCostsLimit(resultSet.getBigDecimal("costs_limit"));
                officer.setBusinessPhone(resultSet.getInt("business_phone"));
                officer.setName(resultSet.getString("name"));
                officer.setPesel(resultSet.getString("pesel"));
                officer.setPosition(PositionEnum.valueOf(resultSet.getString("position")));
                officer.setSurname(resultSet.getString("surname"));
                officer.setSallary(resultSet.getBigDecimal("sallary"));

                oficerList.add(officer);
            }

            return oficerList;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NullPointerException e){
            System.out.println(e.getMessage());
        }


        return null;
    }
}