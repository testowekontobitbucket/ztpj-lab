package pl.edu.zut.ztpjlab2.DAOs;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import pl.edu.zut.ztpjlab2.Entities.Employee;
import pl.edu.zut.ztpjlab2.Entities.Salesman;
import pl.edu.zut.ztpjlab2.Enum.PositionEnum;
import pl.edu.zut.ztpjlab2.Service.DatabaseService;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.Optional;

public class SalesmanDAO implements DAO {
    private final String DB_NAME = "salesman";

    @Override
    public void create(Object o) {
        Salesman salesman = (Salesman) o;

        PreparedStatement statement;

        try {
        statement = DatabaseService.getConnection().prepareStatement(
                "INSERT INTO " + DB_NAME +
                "(`pesel`, `name`, `surname`, `position`, `sallary`, `business_phone`, `commission_rate`, `commission_limit_per_month`)" +
                " VALUES (?, ?, ?, ?, ?, ?, ?, ?)"
        );
        statement.setString(1, salesman.getPesel());
        statement.setString(2, salesman.getName());
        statement.setString(3, salesman.getSurname());
        statement.setString(4, salesman.getPosition().name());
        statement.setBigDecimal(5, salesman.getSallary());
        statement.setInt(6, salesman.getBusinessPhone());
        statement.setBigDecimal(7, salesman.getCommissionRate());
        statement.setBigDecimal(8, salesman.getCommissionLimitPerMonth());

        statement.executeUpdate();
        } catch (MySQLIntegrityConstraintViolationException e) {
            System.out.println(e.getMessage());
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional read(long id) {
        return Optional.empty();
    }

    @Override
    public void update(Object o, String[] params) {

    }

    @Override
    public Boolean delete(Object o) {
        PreparedStatement statement;

        try {
            statement = DatabaseService.getConnection().prepareStatement(
                    "DELETE FROM " + DB_NAME + " WHERE pesel = ?"
            );
            statement.setString(1, ((Employee)o).getPesel());

            int result = statement.executeUpdate();
            return result == 1 ? Boolean.TRUE : Boolean.FALSE;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Boolean.FALSE;
    }

    @Override
    public LinkedList readAll() {
        PreparedStatement statement;
        ResultSet resultSet;
        LinkedList<Salesman> salesmanList = new LinkedList<>();

        try {
            statement = DatabaseService.getConnection().prepareStatement(
                    "SELECT * FROM " + DB_NAME
            );
            resultSet = statement.executeQuery();

            while(resultSet.next()) {
                Salesman salesman = new Salesman();
                salesman.setCommissionRate(resultSet.getBigDecimal("commission_rate"));
                salesman.setCommissionLimitPerMonth(resultSet.getBigDecimal("commission_limit_per_month"));
                salesman.setBusinessPhone(resultSet.getInt("business_phone"));
                salesman.setName(resultSet.getString("name"));
                salesman.setPesel(resultSet.getString("pesel"));
                salesman.setPosition(PositionEnum.valueOf(resultSet.getString("position")));
                salesman.setSurname(resultSet.getString("surname"));
                salesman.setSallary(resultSet.getBigDecimal("sallary"));

                salesmanList.add(salesman);
            }

            return salesmanList;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }
}
